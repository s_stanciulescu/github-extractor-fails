package json

import org.junit._
import org.junit.Assert._
import scala.io.Source
import dk.itu.scas.github.extractor.GithubExtractor._
import dk.itu.scas.github.extractor._
import dk.itu.scas.github.extractor.util._
import argonaut._, Argonaut._
import java.io.File
import java.util.NoSuchElementException


class UserDecoder {

 val userNoName = Source.fromFile(new File("resources/user-no-name.json")).getLines.mkString
 val userNoEmail = Source.fromFile(new File("resources/user-no-email.json")).getLines.mkString
 val userNoEmailNoName = Source.fromFile("resources/user-no-email-no-name.json").getLines.mkString
 val user = Source.fromFile("resources/user.json").getLines.mkString
 

 @Test( )
 def parseUserNoNamePass(){
  val option : Option[User] = Parse.decodeOption[User](userNoName)
  
 }
}