package json


import org.junit.Test
import org.junit.Assert._
import argonaut._, Argonaut._
import dk.itu.scas.github.extractor.GithubExtractor._
import dk.itu.scas.github.extractor._
import dk.itu.scas.github.extractor.util._

class PullRequestDecoding {

  val pullRequest1 = Util.readFromFile("resources/pull-request.json")
  val pullRequest2 = Util.readFromFile("resources/pull-request-1.json")
  
  @Test
  def testParsingPullRequest {
   assertTrue(pullRequest1.parse.validation.isSuccess) 
   assertTrue(pullRequest2.parse.validation.isSuccess)
  }
  
  @Test
  def testDecodingSuccessful(){
   assertTrue(pullRequest1.decodeValidation[SinglePullRequest].isSuccess)
   assertTrue(pullRequest2.decodeValidation[SinglePullRequest].isSuccess)
  }
  
  @Test 
  def testDecoding(){
    assertTrue(pullRequest1.decodeOption[SinglePullRequest].get.mergeable.get)
    assertFalse(pullRequest1.decodeOption[SinglePullRequest].get.merged)
  }
  
  @Test 
  def testFromRepo(){
    val pullRequest = pullRequest1.decodeOption[SinglePullRequest].get
    val fullName = pullRequest.head.get.repo match{
      case Some(SimpleRepository(_,_)) => pullRequest.head.get.repo.get.full_name
      case None => "unknown"
    }
    assertEquals("unknown", fullName)
    assertEquals("StephS/Sprinter", pullRequest2.decodeOption[SinglePullRequest].get.head.get.repo.get.full_name)
  }
}