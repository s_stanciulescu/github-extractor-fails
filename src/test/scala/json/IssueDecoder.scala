package json

import org.junit.Test
import org.junit.Assert._
import dk.itu.scas.github.extractor._
import dk.itu.scas.github.extractor.util.Util._
import argonaut._
import Argonaut._
class IssueDecoder {

  val issues = readFromFile("resources/issues2.json")
  
  @Test
  def decodeIssues(){
    val result = issues.decodeValidation[List[Issues]]
    assertTrue(result.isSuccess)
  }
}