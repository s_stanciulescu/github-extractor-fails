package json

import org.junit.Test
import org.junit.Assert._
import scala.io.Source
import java.io.File
import dk.itu.scas.github.extractor.GithubExtractor._
import dk.itu.scas.github.extractor._
import dk.itu.scas.github.extractor.util.Util._


class RepoDecoder {

 val json = readFromFile("resources/repo.json")
 
 @Test
 def parseJson(){
  val repoJson = getRepositoryJson("tonokip/Tonokip-Firmware")
  assertEquals(prettyPrintJson(json), repoJson._1.mkString)
 }
}