package json

import org.junit.Test
import org.junit.Assert._
import dk.itu.scas.github.extractor._
import dk.itu.scas.github.extractor.util.Util._
import argonaut._
import Argonaut._
class CommentsDecoder {

  val comments1 = readFromFile("resources/comments1.json")
  val comments2 = readFromFile("resources/comments2.json")
  val comments3 = readFromFile("resources/comments3.json")
  
  @Test
  def decodeComments{
    val result1 = comments1.decodeValidation[List[Comments]]
    val result2 = comments2.decodeValidation[List[Comments]]
    val result3 = comments2.decodeValidation[List[Comments]]
    assertTrue(result1.isSuccess)
    assertTrue(result2.isSuccess)
    assertTrue(result3.isSuccess)
  }
}