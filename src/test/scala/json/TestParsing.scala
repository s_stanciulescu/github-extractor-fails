package json

import org.junit._
import org.junit.Assert._
import dk.itu.scas.github.extractor.GithubExtractor._
import dk.itu.scas.github.extractor._
import dk.itu.scas.github.extractor.util.Util._

import argonaut._
import Argonaut._

class TestParsing {

 val parse1 = readFromFile("resources/branches.json")
 val parse2 = readFromFile("resources/Marlin_v1-commits.json")
 @Test
 def test(){
   val parse = Parse.parse(parse1)
   
   assertEquals(true, parse.validation.isSuccess)
 }
 
 @Test
 def errorParse(){
  val parse = Parse.parse(parse2)
  assertEquals(false, parse.validation.isSuccess)
 }
}