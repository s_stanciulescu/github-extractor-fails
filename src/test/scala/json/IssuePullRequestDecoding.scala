package json

import org.junit.Test
import org.junit.Assert._
import dk.itu.scas.github.extractor.GithubExtractor._
import dk.itu.scas.github.extractor._
import dk.itu.scas.github.extractor.util._
import argonaut._
import Argonaut._
class IssuePullRequestDecoding {

  val issues = getUsingPages("kliment/Sprinter", "issues", "","")
  @Test
  def parseIssues(){
    val result = issues._1.decodeValidation[List[Issues]] 
    assertTrue(result.isSuccess)
  }
  
}