package patch

import org.junit.Test
import org.junit.Assert._
class GetPatch {

  
  
  @Test
  def tryPatch(){
    val patch = dk.itu.scas.github.extractor.GithubExtractor.getPatch("Smoothieware/Smoothieware","https://github.com/Smoothieware/Smoothieware/pull/65.patch")
    
    assertEquals(patch, "Content containing PDF or PS header bytes cannot be rendered from this domain for security reasons.")
  }
}