package util

import org.junit.Test
import org.junit.Assert._
class ReadFromFile {

  
  @Test
  def readFromFile(){
    val databaseSettings = dk.itu.scas.github.extractor.util.Util.readFromFileLines("resources/database.txt")
    val expectedUser = "scas"
    val expectedPass = "scasPass"
    val expectedURL = "localhost"
    val expectedPort = "3306"
    
    assertEquals(databaseSettings(0).split(":")(1), expectedURL)
    assertEquals(databaseSettings(1).split(":")(1), expectedPort)
    assertEquals(databaseSettings(2).split(":")(1), expectedUser)
    assertEquals(databaseSettings(3).split(":")(1), expectedPass)
  }
}