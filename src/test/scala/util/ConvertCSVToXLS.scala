package util

import org.junit.Test
import org.junit.Assert._
import util.Util
import au.com.bytecode.opencsv.bean.CsvToBean
import dk.itu.scas.github.extractor._
import dk.itu.scas.github.extractor.util._

class ConvertCSVToXLS {

 @Test
 def convert(){
  val input = "resources/util/alive-forks.csv"
  CsvToXls.convertCSVToXLS(input, "resources/util/conversion.xls")
 }
}