package util

import org.joda.time.DateTime
import argonaut._
import Argonaut._
import org.junit.Test
import org.junit.Assert._

import dk.itu.scas.github.extractor._
import dk.itu.scas.github.extractor.util._

class Util {

 val date = new DateTime("2011-02-24T10:09:08.000-05:00")
 val branchCommits = Util.readFromFile("resources/util/master-commits.json").decodeOption[List[Commits]].get
 
 val expected = Nil
 val actual = branchCommits.takeWhile(p => new DateTime(p.commit.get.committer.get.date).isAfter(date))
 
 
 @Test
 def testNoCommitAfter(){
  assertEquals(0, actual.size)
 // assertNull(actual)
 }
 @Test
 def testCommitBefore(){
  val actual = branchCommits.takeWhile(p => new DateTime(p.commit.get.committer.get.date).isBefore(date))
  assertEquals(11, actual.size)
 }
 
 @Test
 def testCommitsAfterForkDate(){
   val commits = Util.readFromFile("resources/util/Marlin_v1-commits.json").decodeOption[List[Commits]].get
   val nrOfCommitsAfterForkDate = 9
   val forkDate = new DateTime("2013-12-10T23:04:35Z")
   val commitsAfterForkDate = commits.takeWhile( p => new DateTime(p.commit.get.committer.get.date).isAfter(forkDate))
   assertEquals(9, commitsAfterForkDate.size)
   
 }
 
}