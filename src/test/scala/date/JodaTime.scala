package date

import org.junit.Test
import org.joda.time.DateTime
import org.junit.Assert
class JodaTime {
	
  @Test
  def testIfBefore(){
   val time1 = new DateTime("2014-10-11T21:10:00Z")
   val time2 = new DateTime("2014-10-10T21:10:00Z")
    
    val isBefore = time2.isBefore(time1)
    Assert.assertEquals(true, isBefore)
    
    
  }
  @Test
  def isAfter(){
   val time1 = new DateTime("2014-10-11T21:10:00Z")
   val time2 = new DateTime("2014-10-10T21:10:00Z")
   val isAfter = time1.isAfter(time2)
   Assert.assertEquals(true, isAfter)
  }
  
  
  @Test
  def isNotAfter{
   val time1 = new DateTime("2014-10-11T21:10:00Z")
   val time2 = new DateTime("2014-10-10T21:10:00Z")
    
   Assert.assertEquals(true, time1.isAfter(time2))
  }
  
}