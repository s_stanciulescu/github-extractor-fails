create table if not exists branches(
	id INT PRIMARY KEY auto_increment,
	branch_name VARCHAR(100),
	repo_id INT,
	nr_commits INT,
	last_commit_sha VARCHAR(42)
);