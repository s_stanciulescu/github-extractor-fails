ALTER TABLE branches_commits
ADD CONSTRAINT  fk_branch_id_constraint
	FOREIGN KEY fk_branch_id (branch_id)
	REFERENCES branches(id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE;