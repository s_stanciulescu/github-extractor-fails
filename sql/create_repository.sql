create table if not exists repository(
	id INT  PRIMARY KEY auto_increment,
	github_id INT,
	repo_name VARCHAR(100),
	created_at VARCHAR(30),
	updated_at VARCHAR(30),
	pushed_at  VARCHAR(30),	
	repo_owner_login VARCHAR(100),
    repo_owner_name VARCHAR(100),
    repo_owner_email VARCHAR(100),
	forks_count INT,
	forked_from VARCHAR(100), #this should be the full_name from repo's json
	fork_level INT,
	active_fork BOOL
);
