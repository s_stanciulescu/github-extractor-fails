create table if not exists issues(
	id INT  PRIMARY KEY auto_increment,
	github_id INT,
	repo_id INT,
	repo_name VARCHAR(100),
	issue_number INT,
	title VARCHAR(500),
	user_login VARCHAR(100),
	user_github_id INT,
	state VARCHAR(30),
	nr_of_comments LONG,
	created_at VARCHAR(30),
	updated_at VARCHAR(30), 
	closed_at VARCHAR(30),
	body LONGTEXT
);
